# README #

After you've installed all the applications needed to start working on the backend project, fork or clone the repository that I've sent you before to start practicing your skills in working with Java programming. Accomplish the following items by creating these models:

## User ##

* Basic CRUD (GET, POST, PUT, PATCH, DELETE)
* Create search endpoint for searching users by company_name
* Create search endpoint for searching users by state
* Create search endpoint for searching users where address containing string

## User Address ##

* Basic CRUD (GET, POST, PUT, PATCH, DELETE)
* Create search endpoint for searching user_address where address containing string
* Create search endpoint for searching user_address where region equals
* Create search endpoint for searching user_address where country equals

## Apps to Install: ##
* MySQL community server 8
* MySQL Workbench
* Sourcetree

### References: ###
* Spring Data Rest Reference: - https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation
