package com.gmconnect.user.repository;

import com.gmconnect.user.model.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {

    Page<User>findByCityOrStateEquals(String city, String state, Pageable pageable);
    
}
